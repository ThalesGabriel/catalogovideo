package com.fullcycle.CatalogoVideo.domain.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.fullcycle.CatalogoVideo.domain.entity.Category;

public interface ICategoryRepository {
    List<Category> findAll();
    Category create(Category category);
    Optional<Category> getById(UUID id);
    void delete(UUID id);
    void update(Category category);
}
