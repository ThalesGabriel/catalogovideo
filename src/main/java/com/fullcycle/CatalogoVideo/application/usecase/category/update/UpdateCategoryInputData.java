package com.fullcycle.CatalogoVideo.application.usecase.category.update;

import com.fullcycle.CatalogoVideo.domain.entity.Category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCategoryInputData {
    private String name;
    private String description;
    private Boolean isActive = true;

    public Category toCategory() {
        return new Category(this.name, this.description, this.isActive);
    }
}
