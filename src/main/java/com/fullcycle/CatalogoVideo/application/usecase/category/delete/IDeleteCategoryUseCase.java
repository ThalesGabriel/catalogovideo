package com.fullcycle.CatalogoVideo.application.usecase.category.delete;

import java.util.UUID;

public interface IDeleteCategoryUseCase {
    void execute(UUID id);
}
