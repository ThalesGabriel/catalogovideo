package com.fullcycle.CatalogoVideo.application.usecase.category.update;

import java.util.UUID;

import com.fullcycle.CatalogoVideo.application.exception.NotFoundException;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCategoryUseCase implements IUpdateCategoryUseCase {

    private ICategoryRepository repository;

    @Override
    public void execute(UUID id, UpdateCategoryInputData inputData) {
        Category category = repository.getById(id)
            .orElseThrow(() -> new NotFoundException("Category %s not found.", id));

        category.update(inputData.getName(), inputData.getDescription(), inputData.getIsActive());

        repository.update(category);
    }

}
