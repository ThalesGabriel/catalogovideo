package com.fullcycle.CatalogoVideo.application.usecase.category.delete;

import java.util.UUID;

import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class DeleteCategoryUseCase implements IDeleteCategoryUseCase {

    private ICategoryRepository repository;

    @Override
    public void execute(UUID id) {
        repository.delete(id);
    }
    
}
