package com.fullcycle.CatalogoVideo.application.usecase.category.findall;

import java.util.List;
import java.util.stream.Collectors;

import com.fullcycle.CatalogoVideo.application.usecase.category.common.CategoryOutputData;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class FindAllCategoryUseCase implements IFindAllCategoryUseCase {

    private ICategoryRepository repository;

    @Override
    public List<CategoryOutputData> execute() {
        List<Category> list = repository.findAll();

        return list.stream()
            .map(c -> CategoryOutputData.fromCategory(c))
            .collect(Collectors.toList());
    }
    
}
