package com.fullcycle.CatalogoVideo.application.usecase.category.get;

import java.util.UUID;

import com.fullcycle.CatalogoVideo.application.exception.NotFoundException;
import com.fullcycle.CatalogoVideo.application.usecase.category.common.CategoryOutputData;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class GetCategoryUseCase implements IGetCategoryUseCase {

    private ICategoryRepository repository;

    @Override
    public CategoryOutputData execute(UUID id) throws Exception {
        return repository.getById(id)
            .map(CategoryOutputData::fromCategory)
            .orElseThrow(() -> new NotFoundException("Category %s not found.", id));
    }
    
}
