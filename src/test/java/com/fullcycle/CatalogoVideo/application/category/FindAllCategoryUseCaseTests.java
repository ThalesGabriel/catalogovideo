package com.fullcycle.CatalogoVideo.application.category;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.fullcycle.CatalogoVideo.application.usecase.category.common.CategoryOutputData;
import com.fullcycle.CatalogoVideo.application.usecase.category.findall.FindAllCategoryUseCase;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class FindAllCategoryUseCaseTests {
    
    @InjectMocks
    private FindAllCategoryUseCase useCase;

    @Mock
    ICategoryRepository repository;

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_DESCRIPTION = "Description 1";
    private final String CATEGORY_NAME_2 = "Name 2";
    private final String CATEGORY_DESCRIPTION_2 = "Description 2";
    private final String CATEGORY_NAME_3 = "Name 3";
    private final String CATEGORY_DESCRIPTION_3 = "Description 3";

    @BeforeEach
    void initUseCase() {
        useCase = new FindAllCategoryUseCase(repository);
    }

    @Test
    public void findAll() {
        List<Category> categories = Arrays.asList(
            new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION),
            new Category(CATEGORY_NAME_2, CATEGORY_DESCRIPTION_2),
            new Category(CATEGORY_NAME_3, CATEGORY_DESCRIPTION_3)
        );

        when(repository.findAll()).thenReturn(categories);
        
        List<CategoryOutputData> outputData = useCase.execute();
        repository.findAll();

        assertNotNull(categories);
        assertThat(categories).hasSize(3);
        // verify(repository, times(1)).findAll();

        assertNotNull(outputData);
        assertThat(outputData).hasSize(3);
    }
}
