package com.fullcycle.CatalogoVideo.application.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fullcycle.CatalogoVideo.application.usecase.category.common.CategoryOutputData;
import com.fullcycle.CatalogoVideo.application.usecase.category.create.CreateCategoryInputData;
import com.fullcycle.CatalogoVideo.application.usecase.category.create.CreateCategoryUseCase;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class CreateCategoryUseCaseTests {

    @InjectMocks
    private CreateCategoryUseCase useCase;

    @Mock
    ICategoryRepository repository;

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_DESCRIPTION = "Description 1";
    private final Category category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);


    @BeforeEach
    void initUseCase() {
        useCase = new CreateCategoryUseCase(repository);
    }

    @Test
    public void createCategory() {

        when(repository.create(any(Category.class)))
            .thenReturn(category);

        CreateCategoryInputData inputData = new CreateCategoryInputData(
            category.getName(),
            category.getDescription(),
            category.getIsActive()
        );

        CategoryOutputData outputData = useCase.execute(inputData);
        repository.create(category); 
        
        assertEquals(outputData.getName(), category.getName());
        assertEquals(outputData.getDescription(), category.getDescription());
        assertEquals(outputData.getIsActive(), category.getIsActive());

    }

}
