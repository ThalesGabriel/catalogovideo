package com.fullcycle.CatalogoVideo.application.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.fullcycle.CatalogoVideo.application.usecase.category.update.UpdateCategoryInputData;
import com.fullcycle.CatalogoVideo.application.usecase.category.update.UpdateCategoryUseCase;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class UpdateCategoryUseCaseTests {
    
    @InjectMocks
    private UpdateCategoryUseCase useCase;

    @Mock
    ICategoryRepository repository;

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_DESCRIPTION = "Description 1";
    private final String CATEGORY_NAME_2 = "Name 2";
    private final String CATEGORY_DESCRIPTION_2 = "Description 2";

    @BeforeEach
    void initUseCase() {
        useCase = new UpdateCategoryUseCase(repository);
    }

    @Test
    public void getById() throws Exception {
        Category category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        Category categoryUpdated = new Category(CATEGORY_NAME_2, CATEGORY_DESCRIPTION_2);

        Optional<Category> optional = Optional.of(category);

        when(repository.getById(category.getId())).thenReturn(optional);
        
        UpdateCategoryInputData inputData = new UpdateCategoryInputData();
        inputData.setName(categoryUpdated.getName());
        inputData.setDescription(categoryUpdated.getDescription());
        inputData.setIsActive(categoryUpdated.getIsActive());

        category.update(inputData.getName(), inputData.getDescription(), inputData.getIsActive());

        doNothing()
            .when(repository)
            .update(category);

        useCase.execute(category.getId(), inputData);
        
        assertNotNull(category);
        assertNotNull(categoryUpdated);
        assertEquals(category.getName(), categoryUpdated.getName());
    }
}