package com.fullcycle.CatalogoVideo.application.category;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.fullcycle.CatalogoVideo.application.usecase.category.common.CategoryOutputData;
import com.fullcycle.CatalogoVideo.application.usecase.category.get.GetCategoryUseCase;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class GetCategoryUseCaseTests {
    
    @InjectMocks
    private GetCategoryUseCase useCase;

    @Mock
    ICategoryRepository repository;

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_DESCRIPTION = "Description 1";

    @BeforeEach
    void initUseCase() {
        useCase = new GetCategoryUseCase(repository);
    }

    @Test
    public void getById() throws Exception {
        Category category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        Optional<Category> optional = Optional.of(category);

        when(repository.getById(category.getId())).thenReturn(optional);
        
        CategoryOutputData outputData = useCase.execute(category.getId());
        repository.getById(category.getId());

        assertNotNull(category);
        assertNotNull(outputData);

    }
}