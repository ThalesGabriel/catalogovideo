package com.fullcycle.CatalogoVideo.application.category;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.fullcycle.CatalogoVideo.application.usecase.category.delete.DeleteCategoryUseCase;
import com.fullcycle.CatalogoVideo.domain.entity.Category;
import com.fullcycle.CatalogoVideo.domain.repository.ICategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class DeleteCategoryUseCaseTests {
    
    @InjectMocks
    private DeleteCategoryUseCase useCase;

    @Mock
    ICategoryRepository repository;

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_DESCRIPTION = "Description 1";

    @BeforeEach
    void initUseCase() {
        useCase = new DeleteCategoryUseCase (repository);
    }

    @Test
    public void deleteById() throws Exception {
        Category category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);

       doNothing().when(repository).delete(category.getId());

       useCase.execute(category.getId());
       repository.delete(category.getId());

       assertNotNull(category);
       verify(repository, times(2)).delete(category.getId());
    }
}