package com.fullcycle.CatalogoVideo.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fullcycle.CatalogoVideo.domain.exception.DomainException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CategoryTests {

    private final String CATEGORY_NAME = "Name 1";
    private final String CATEGORY_NAME_2 = "Name 2";
    private final String CATEGORY_DESCRIPTION = "Description 1";
    private final String CATEGORY_DESCRIPTION_2 = "Description 2";

    private Category category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);

    @Test
    public void createCategory() {

        assertNotNull(category);
        assertNotNull(category.getId());
        assertEquals(CATEGORY_NAME, category.getName());
        assertEquals(CATEGORY_DESCRIPTION, category.getDescription());
        assertEquals(true, category.getIsActive());
    }

    @Test
    public void createCategoryAndDeactive() {

        category.deactivate();
        
        assertNotNull(category);
        assertFalse(category.isIsActive());
    }

    @Test
    public void createCategoryAndUpdate() {
        category = new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        assertNotNull(category);
        assertNotNull(category.getId());
        assertEquals(CATEGORY_NAME, category.getName());
        assertEquals(CATEGORY_DESCRIPTION, category.getDescription());
        assertTrue(category.getIsActive());

        category.update(CATEGORY_NAME_2, CATEGORY_DESCRIPTION_2, false);

        assertNotNull(category);
        assertNotNull(category.getId());
        assertEquals(CATEGORY_NAME_2, category.getName());
        assertEquals(CATEGORY_DESCRIPTION_2, category.getDescription());
        assertFalse(category.getIsActive());
    }

    @Test
    public void throwDomainExceptionWhenNameIsNull() {
        assertThrows(DomainException.class, () -> new Category(null, CATEGORY_DESCRIPTION));
    }

    @Test
    public void throwDomainExceptionWhenNameIsBlank() {
        assertThrows(DomainException.class, () -> new Category("", CATEGORY_DESCRIPTION));
    }
    
}
