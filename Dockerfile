from openjdk:11

RUN useradd -ms /bin/bash gradle
VOLUME "/home/gradle/.gradle"
WORKDIR /home/gradle
